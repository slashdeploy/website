FROM ruby:2.2

ENV LC_ALL C.UTF-8

RUN mkdir -p /usr/src/app/vendor
WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock /usr/src/app/
COPY vendor/cache vendor/cache
RUN bundle install --local -j $(nproc)

CMD [ "bundle", "exec", "jekyll" ]
