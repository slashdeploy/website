---
layout: default
---

Our passion is continuous deployment down to the nitty gritty
infastructure, tooling, automation, to the high level business impact.
The goal is to make get your business in a position to build the
highest quality software with short iteration times and sustainable
velocity.

Learn more about us by reading our [blog][]. Bootstrapping a new
project? Checkout the [Docker project boilerplates][boilerplate]. Need
to understand Ruby web applications? Take the [Rack Bootcamp][]. Want
to learn about shell scripting? Start with the [Bash Bootcamp][]. Here
are some more helpful links from across the internet.

**Blog Posts, Articles, & Tutorials**

{% for item in site.data.external_links %}
* {{ item.date }} - [{{ item.title }}]({{ item.url }})
{% endfor %}

**Webinars**

{% for item in site.data.webinars %}
* {{ item.date }} - [{{ item.title }}]({{ item.url }})
{% endfor %}

**Courses**

{% for item in site.data.courses %}
* {{ item.date }} - [{{ item.title }}]({{ item.url }})
{% endfor %}

**Talks**

{% for item in site.data.talks %}
* {{ item.event }} - [{{ item.title }}]({{ item.link }})
{% endfor %}

[blog]: http://blog.slashdeploy.com
[bash bootcamp]: {{ site.bash_bootcamp_landing_page }}
[boilerplate]: {% link boilerplate.md %}
[rack bootcamp]: {{ site.rack_bootcamp_landing_page }}
