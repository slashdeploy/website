---
layout: default
title: Docker Project Boilerplate
permalink: boilerplate.html
---

## Why?

Slashdeploy is passionate about continuous deployment. Continuous
deployment is the best way to build quality software. Achieving
continuous deployment is no small task. Reaching the goal requires
attention to detail at every step in the software process. Software
starts in the development phase. The outcome here is simple: create
software that works as expected and that there are no regressions.

Test Driven Development (or TDD for short) is the best way to achieve
this outcome. The highest quality software is created by applying TDD
to classes, functions, modules, _and_ the other software artifacts.
Most teams cover the first set of things but drop out on the second.
How many times have you deployed code to production/staging/etc and
seen errors because of some missing configuration or something that's
conditional to that stage? How many times have you seen your software
fail because it was never actually _started_ before going to
production? Each of these failures (and others) are just like any
other bug. They can be provably solved with regression tests.

## Structure

This is where Docker and the associated boilerplate code come into the
picture. Working with containers makes encapsulating the development
and testing process easy for all types of testing. This is why we're
sharing these project boilerplate repos. They provide an excellent
structure to build a production ready project. The boilerplates are
build on `make`, `docker`, and `docker-compose`. Each boilerplate includes:

* Docker based dependency management
* TDD setup
* Code linting
* An [editorconfig][] file configured for best practices
* Dockerfile configured to run a web server
* Blackbox & whitebox tests for the web server
* `make test-ci`: Run a full battery of tests
* `make test-smoke`: Run a smoke test against production process

They generally follow the same structure. `docker` is used to generate
dependencies so they can be committed to source control. This removes
upstream dependencies on upstream package hosts at build time.
`docker-compose` manages containers for various other targets. One
container runs the process (the boilerplates use a simple web
server as a common case). Another container is used for quick tests
(for interpreted languages which don't require a separate compile
step). Another container is used to smoke test the application
container.

## Boilerplate Templates

All boilerplates are free to use and customize. Simply download or
clone the repo and follow the `TODO`'s inside. The repositories are
continually updated to follow best practices learned from using Docker
to ship the highest quality software possible.

* [Ruby][]
* [Node.js][]

Feedback and pull requests welcome as always. Good luck out there and
happy shipping!

[blog]: http://blog.slashdeploy.com
[editorconfig]: http://editorconfig.org
[ruby]: https://gitlab.com/slashdeploy/docker-ruby-boilerplate
[node.js]: https://gitlab.com/slashdeploy/docker-node-boilerplate
