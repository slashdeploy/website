#!/usr/bin/env bats

function setup() {
	[ -n "${DIST_PATH}" ]
}

@test "index.html exists" {
	[ -f "${DIST_PATH}/index.html" ]
}

@test "error.html exists" {
	[ -f "${DIST_PATH}/error.html" ]
}

@test "error.html is not searchable" {
	grep -qF "Disallow: /error.html" "${DIST_PATH}/robots.txt"
}

@test "sitemap.xml exists" {
	[ -f "${DIST_PATH}/sitemap.xml" ]
}

@test "sitemap set" {
	grep -qE 'Sitemap:\shttps?:\/\/.+\/sitemap\.xml' "${DIST_PATH}/robots.txt"
}

@test "tracking code added" {
	while read -r file; do
		grep -qF 'https://track.gaug.es/track.gif' "${file}"
	done < <(find "${DIST_PATH}" -iname '*.html' -print)
}
