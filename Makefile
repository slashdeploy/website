DOCKER_IMAGE:=tmp/image
SITE:=dist/index.html
BUILD_ENV:=-e JEKYLL_ENV=production

.DEFAULT_GOAL:=dist

.PHONY: check
check:
	jq --version
	aws --version
	docker --version
	bats --version

Gemfile.lock: Gemfile
	docker run --rm -v $(CURDIR):/data -w /data ruby:2.2 \
		bundle package

$(DOCKER_IMAGE): Dockerfile Gemfile.lock
	docker build -t slashdeploy/website .
	mkdir -p $(@D)
	touch $@

.PHONY: init
init: $(DOCKER_IMAGE)
	docker run --rm -v $(CURDIR):/data -w /data slashdeploy/website \
		bundle exec jekyll new src

$(SITE): $(DOCKER_IMAGE)
	mkdir -p dist tmp
	docker run --rm -u $(shell id -u) -v $(CURDIR):/data -w /data $(BUILD_ENV) slashdeploy/website \
		bundle exec jekyll build -d /data/dist -s /data/src

.PHONY: dist
dist: $(SITE)

.PHONY: test-dist
test-dist: $(SITE)
	env DIST_PATH=$(CURDIR)/dist test/dist_test.bats
	docker run --rm -v $(CURDIR)/dist:/data slashdeploy/website \
		bundle exec htmlproofer /data --internal-domains slashdeploy.com

.PHONY: test-shellcheck
test-shellcheck:
	docker run --rm -v $(CURDIR):/data -w /data jrotter/shellcheck \
		shellcheck -s bash \
		$(shell find bin script -type f -exec test -x {} \; -print | paste -s -d ' ' -)

.PHONY: test-website
test-website:
	bin/website validate

.PHONY: test-ci
test-ci:
	$(MAKE) test-shellcheck
	$(MAKE) test-dist
	$(MAKE) test-website

.PHONY: clean
clean:
	rm -rf $(DOCKER_IMAGE) dist
